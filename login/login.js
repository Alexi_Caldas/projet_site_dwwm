$("form").submit((e) => { 
    e.preventDefault();
    
    $.ajax({
        url: "login.php",
        type: "POST",
        dataType: "json",
        data: {
            email: $("#email").val(),
            pwd: $("#pwd").val()
        },
        success: (result) => {
            if (result.success) {
                window.location.replace("../home/home.html");
                localStorage.setItem("user", JSON.stringify(result.user));
            } else alert(result.error); 
        }
    });
});