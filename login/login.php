<?php

session_start();

require_once("../db_connect.php");

if (
    !isset($_POST["email"], $_POST["pwd"]) ||
    empty(trim($_POST["email"])) || empty(trim($_POST["pwd"]))
    
) {
    echo json_encode(["success" => false, "error" => "Caractère non conforme"]);
}
$req = $db->prepare("SELECT * FROM user WHERE email = ?");
$req->execute([$_POST["email"]]);

$user = $req->fetch(PDO::FETCH_ASSOC);

if ($user && password_verify($_POST["pwd"], $user["pwd"])) {
    $_SESSION["connected"] = true;
    $_SESSION["user_id"] = $user["id"];
    $_SESSION["admin"] = $user["admin"];
    echo json_encode(["success" => true, "user" => $user]);
    
} else { 
    $_SESSION = [];
    echo json_encode(["success" => false, "error" => "Utilisateur introuvable"]);
}