<?php

session_start();

function isAdmin() {
    if (!isset($_SESSION["admin"]) || !$_SESSION["admin"]) {
        echo json_encode(["success" => false, "error" => "Vous n'êtes pas autorisé"]);
        die;
    }
}

function isConnected() {
        if (!isset($_SESSION["connected"]) || !$_SESSION["connected"]) {
        echo json_encode(["success" => false, "error" => "Vous n'êtes pas connecté"]);
        die;
    }
}
