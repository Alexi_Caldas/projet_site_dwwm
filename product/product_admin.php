<?php
require_once("../db_connect.php");
require("../function.php");

isConnected();

isAdmin();

if ($_SERVER["REQUEST_METHOD"] == "POST") $method = $_POST;
else $method = $_GET;

switch ($method["choice"]) {
    case 'select':
        $req = $db->query("SELECT * FROM product p INNER JOIN category c ON c.id = p.id_category");
        $product = $req->fetchAll(PDO::FETCH_ASSOC);
        echo json_encode(["success" => true, "product" => $product]);
        break;
    
    case 'select_id':
        if (isset($_GET["id"])) {
            $req = $db->prepare("SELECT * FROM product WHERE id = ?");
            $req->execute([$_GET["id"]]); 
            $product = $req->fetch(PDO::FETCH_ASSOC);
            echo json_encode(["success" => true, "product" => $product]);

        } else {
            echo json_encode(["success" => false, "error" => "Erreur lors de la sélection de l'article"]);
        }
        break;

    case 'insert':
        if (isset($_POST["name"], $_POST["reference"]) && !empty(trim($_POST["name"])) && !empty(trim($_POST["reference"]))) {

            $req = $db->prepare("INSERT INTO product (`name`, reference, price_t, id_category) VALUES (:name, :reference, :price_t, :id_category)");

            $req->bindValue(":name", $_POST["name"]);
            $req->bindValue(":reference", $_POST["reference"]);
            $req->bindValue(":price_t", $_POST["price_t"]);
            $req->bindValue(":id_category", $_POST["id_category"]);
            $req->execute(); 
            echo json_encode(["success" => true]);

        } else { 
            echo json_encode(["success" => false, "error" => "Erreur lors de l'insertion"]);
        }
        break;

    case 'update':
        if (isset($_POST["name"], $_POST["reference"]) && !empty(trim($_POST["name"])) && !empty(trim($_POST["reference"])) ) {
            $req = $db->prepare("UPDATE product SET name = ?, reference = ?");
            $req->execute([$_POST["name"], $_POST["reference"]]);
            echo json_encode(["success" => true]);
        } else {
            echo json_encode((["success" => false, "error" => "Erreur lors de la mise à jour"]));
        }
        break;

    case 'delete':
        if (isset($_POST["id"]) && !empty(trim($_POST["id"]))) {
            $req = $db->prepare("DELETE FROM product WHERE id = ?");
            $req->execute([$_POST["id"]]);
            echo json_encode((["success" => true]));

        } else {
            echo json_encode((["success" => false, "error" => "Erreur lors de la suppression"]));
        }
        break;

    default:
        echo json_encode(["success" => false, "error" => "Demande inconnue"]);
        break;
}
