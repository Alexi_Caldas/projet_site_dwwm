const urlParams = new URLSearchParams(window.location.search); // Je récupère les paramètres de mon url
const onlyfav = urlParams.get("onlyfav"); // Je souhaite savoir si j'affiche seulement les articles en favoris

let select = "select"; // Par défaut le choix est d'afficher tous les articles

//? Si onlyfav est à 1
if (onlyfav == 1) {
    select= "select_onlyfav"; // J'affiche seulement les favoris
    $("title").text("Liste des favoris"); // Je change le titre de l'onglet
    $("header h1").text("Liste des favoris"); // Je change le titre principal de la page
}

let article_id = null; // Variable qui nous servira à savoir si nous voulons insérer ou mettre à jour un article

/**
 * @desc Fait appel au php pour inserer un article
 * @param string name - Contient le nom de l'article
 * @param string description - Contient la description de l'article
 * @param file picture - Contient le fichier à upload
 * @return void - Ne retourne rien
 */
function insertArticle(name, description, picture) {
    const fd = new FormData();
    fd.append('choice', "insert");
    fd.append('name', name);
    fd.append('description', description);
    fd.append('picture', picture);

    $.ajax({
        url: "product.php", // URL cible
        type: "POST", // Type de méthode de requête HTTP
        dataType: "json", // Type de réponse attendue        
        data: fd,
        contentType: false,
        processData: false,
        cache: false,
        success: (res) => {
            const article = $("<article></article>"); // Je crée un élement article
            article.addClass("art_box"); // J'ajoute à mon élément article la classe 'art_box' 
            article.attr("id", "article_" + res.id); // J'ajoute à mon élément article l'id 'article_' + son id 

            const title = $("<h2></h2>").text(name); // Je crée un élément h2 et je lui ajoute le texte correspondant au nom de mon article
            
            const ctn_desc = $("<div></div>");
            const desc = $("<p></p>").text(description); // Je crée un élément p et je lui ajoute le texte correspondant à la description de mon article
            desc.addClass("desc");

            let img; // Je déclare la variable img sans valeur;
            if (res.image) img = $("<img>").attr("src", "../assets/article/" + res.image); // Je crée une image et j'affecte la source

            const div = $("<div></div>"); // Je crée un élement div
            const author = $("<p></p>").text(user.firstname + " " + user.lastname); // Je crée un élément p et je lui ajoute le texte correspondant à l'auteur de mon article

            const date = new Date(); // Je crée une nouvelle date avec pour valeur la date de création de mon article

            let day = date.getDate(); // J'attribue à une variable la valeur du jour de la date que j'ai créée.

            if (day < 10) day = '0' + day; // Si le jour est inférieur à 10 je rajoute un 0 devant

            let month = date.getMonth() + 1; // J'attribue à une variable la valeur du mois de la date que j'ai créée. J'ajoute +1 car le mois commence à 0
            if (month < 10) month = '0' + month; // Si le mois est inférieur à 10 je rajoute un 0 devant

            let hours = date.getHours(); // J'attribue à une variable la valeur des heures de la date que j'ai créée.
            if (hours < 10) hours = '0' + hours; // Si l'heure est inférieur à 10 je rajoute un 0 devant

            let min = date.getMinutes(); // J'attribue à une variable la valeur ddes minutes de la date que j'ai créée.
            if (min < 10) min = '0' + min; // Si les minutes sont inférieurs à 10 je rajoute un 0 devant

            // Je crée un élément small auquel j'attribue le texte correspondant à la date de création de mon article au format jj/mm/yyyy hh:mm
            const date_article = $("<small></small>").text(
                day + '/' + month + '/' + date.getFullYear() + ' ' + hours + 'h' + min
            );

            ctn_desc.append(desc, img); // Je crée le conteneur de l'article avec l'image s'il y en a une
            div.append(author, date_article); // J'ajoute les éléments author et date dans ma div
            article.append(title, ctn_desc, div); // J'ajoute les éléments name, desc et div dans mon article
            $("#art_ctn").prepend(article); // J'ajoute mon article dans mon conteneur à article sur le HTML

            const delbtn = $("<button></button>"); // Je crée un élément bouton
            delbtn.html("<i class='fa fa-trash' aria-hidden='true'></i>"); // J'ajoute le contenu du bouton, ici une icone de poubelle
            delbtn.addClass("btn salmon art_btn"); // J'ajoute des classes sur le bouton pour le style

            // J'ajoute un écouteur d'événement clic sur le bouton
            delbtn.click(() => {
                // J'appelle la fonction wantToDelete pour demander la suppression de l'article
                wantToDelete(res.id);
            });

            const updatebtn = $("<button></button>"); // Je crée un élément bouton
            updatebtn.html("<i class='fa fa-pencil' aria-hidden='true'></i>"); // J'ajoute le contenu du bouton, ici une icone de crayon
            updatebtn.addClass("btn ocean art_btn"); // J'ajoute des classes sur le bouton pour le style

            // J'ajoute un écouteur d'événement clic sur le bouton
            updatebtn.click(() => {
                // J'appelle la fonction wantToDelete pour demander la suppression de l'article
                wantToUpdate(res.id);
            });

            article.attr("data-favorite", 0);
            const favorite = $("<button></button>"); // Je crée un élément bouton
            favorite.html("<i class='fa fa-star-o' aria-hidden='true'></i>"); // J'ajoute le contenu du bouton, ici une icone d'étoile
            favorite.addClass("btn art_btn favorite"); // J'ajoute des classes sur le bouton pour le style

            // J'ajoute un écouteur d'événement clic sur le bouton
            favorite.click(() => {
                // J'appelle la fonction favorite pour ajouter ou supprimer l'article en favoris
                manageFavorite(res.id);
            });

            div.append(updatebtn, delbtn, favorite); // J'ajoute mes boutons dans la div de l'article

            $("#overlay").removeClass("open");
            $(".box").removeClass("open");
        }
    });
}

/**
 * @desc Fait appel au php pour mettre à jour un article
 * @param string name - Contient le nom de l'article
 * @param string description - Contient la description de l'article
 * @param file picture - Contient le fichier à upload
 * @return void - Ne retourne rien
 */
function updateArticle(name, description, picture) {
    const fd = new FormData();
    fd.append("choice", "update");
    fd.append("id", article_id);
    fd.append("name", name);
    fd.append("description", description);
    fd.append("picture", picture);
    
    $.ajax({
        url: "../php/article.php", // URL cible
        type: "POST", // Type de méthode de requête HTTP
        dataType: "json", // Type de réponse attendue   
        data: fd,
        contentType: false,
        processData: false,
        cache: false,
        success: (res) => {
            if (res.success) {
                // Je mets les informations de l'article à jour
                $("#article_" + article_id + " h2").text(name);
                $("#article_" + article_id + " .desc").text(description);
            } else alert(res.error); //! J'affiche une boite de dialogue avec l'erreur

            // Je ferme l'overlay et le formulaire
            $("#overlay").removeClass("open");
            $(".box").removeClass("open");
        }
    });
}

/**
 * @desc Fait appel au php pour récupérer les données d'un article
 * @param string id - Contient l'id de l'article
 * @return void - Ne retourne rien
 */
function wantToUpdate(id) {
    $.ajax({
        url: "../php/article.php", // URL cible
        type: "GET", // Type de méthode de requête HTTP
        dataType: "json", // Type de réponse attendue
        data: { // Donnée(s) à envoyer s'il y en a
            choice: "select_id",
            id
        },
        success: (res) => {
            article_id = id; // J'affecte à ma variable article_id l'id de l'article que je souhaite mettre à jour

            $(".box h1").text("Modifier un article"); // Je mets à jour le titre du formulaire

            $("#name").val(res.article.name); // Je prérempli le champs de mon formulaire name
            $("#desc").val(res.article.description); // Je prérempli le champs de mon formulaire description

            $(".box").addClass("open"); // J'affiche mon formulaire
            $("#overlay").addClass("open"); // J'affiche mon overlay
        }
    });
}

/**
 * @desc Fait appel au php pour supprimer un article
 * @param string id - Contient l'id de l'article
 * @return void - Ne retourne rien
 */
function wantToDelete(id) {
    if (confirm("Voulez-vous vraiment supprimer l'article ?")) {
        $.ajax({
            url: "../php/article.php", // URL cible
            type: "POST", // Type de méthode de requête HTTP
            dataType: "json", // Type de réponse attendue
            data: { // Donnée(s) à envoyer s'il y en a
                choice: "delete",
                id
            },
            success: () => {
                $("#article_" + id).remove();
            }
        });
    }
}

/**
 * @desc Fait appel au php pour ajouter ou supprimer un favoris
 * @param string id - Contient l'id de l'article
 * @return void - Ne retourne rien
 */
function manageFavorite(id) {
    // Je récupère le contenu de l'attribut data-favorite pour savoir si l'article est en favoris ou non
    const is_fav = $("#article_" + id).attr("data-favorite");

    $.ajax({
        url: "../php/article.php", // URL cible
            type: "POST", // Type de méthode de requête HTTP
            dataType: "json", // Type de réponse attendue
            data: { // Donnée(s) à envoyer s'il y en a
            choice: "favorite",
            article_id: id,
            favorite: is_fav
        },
        success: (res) => {
            if (res.success) {
                let icon_fav, attr_fav;
                //? Si c'est déjà un favori alors
                if (is_fav == 0) {
                    attr_fav = 1; // Je met l'attribut de favori à 1
                    icon_fav = "<i class='fa fa-star' aria-hidden='true'></i>"; // Je met l'icone d'étoile pleine
                } else {
                    attr_fav = 0; // Je met l'attribut de favori à 0
                    icon_fav = "<i class='fa fa-star-o' aria-hidden='true'></i>"; // Je met l'icone d'étoile vide
                }
                $("#article_" + id).attr("data-favorite", attr_fav);
                $("#article_" + id + " .favorite").html(icon_fav);

                if (onlyfav == 1 && attr_fav == 0) $("#article_" + id).remove();
            } else alert(res.error);
        }
    });
}

// J'effectue un appel AJAX pour récupérer tous les articles
$.ajax({
    url: "../php/article.php", // URL cible
    type: "GET", // Type de méthode de requête HTTP
    dataType: "json", // Type de réponse attendue
    data: { // Donnée(s) à envoyer s'il y en a
        choice: select
    },
    success: (res) => {
        if (res.success) { //? Si la réponse est un succès alors
            res.articles.forEach(art => { // J'itère sur les articles que je récupère
                const article = $("<article></article>"); // Je crée un élement article
                article.addClass("art_box"); // J'ajoute à mon élément article la classe 'art_box' 
                article.attr("id", "article_" + art.id); // J'ajoute à mon élément article l'id 'article_' + son id 

                const name = $("<h2></h2>").text(art.name); // Je crée un élément h2 et je lui ajoute le texte correspondant au nom de mon article
                
                const ctn_desc = $("<div></div>"); // Je crée le conteneur de l'article et l'image
                const desc = $("<p></p>").text(art.description); // Je crée un élément p et je lui ajoute le texte correspondant à la description de mon article
                desc.addClass("desc"); // J'ajoute la classe desc au paragraphe de la description
                
                let img; // Je déclare la variable img sans valeur;
                if (art.image) img = $("<img>").attr("src", art.image); // Je crée une image et j'affecte la source

                const div = $("<div></div>"); // Je crée un élement div
                const author = $("<p></p>").text(art.author); // Je crée un élément p et je lui ajoute le texte correspondant à l'auteur de mon article

                const date = new Date(art.created_at); // Je crée une nouvelle date avec pour valeur la date de création de mon article

                let day = date.getDate(); // J'attribue à une variable la valeur du jour de la date que j'ai créée.
                if (day < 10) day = '0' + day; // Si le jour est inférieur à 10 je rajoute un 0 devant

                let month = date.getMonth() + 1; // J'attribue à une variable la valeur du mois de la date que j'ai créée. J'ajoute +1 car le mois commence à 0
                if (month < 10) month = '0' + month; // Si le mois est inférieur à 10 je rajoute un 0 devant

                let hours = date.getHours(); // J'attribue à une variable la valeur des heures de la date que j'ai créée.
                if (hours < 10) hours = '0' + hours; // Si l'heure est inférieur à 10 je rajoute un 0 devant

                let min = date.getMinutes(); // J'attribue à une variable la valeur ddes minutes de la date que j'ai créée.
                if (min < 10) min = '0' + min; // Si les minutes sont inférieurs à 10 je rajoute un 0 devant

                // Je crée un élément small auquel j'attribue le texte correspondant à la date de création de mon article au format jj/mm/yyyy hh:mm
                const date_article = $("<small></small>").text(
                    day + '/' + month + '/' + date.getFullYear() + ' ' + hours + 'h' + min
                );

                ctn_desc.append(desc, img); // Je crée le conteneur de l'article avec l'image s'il y en a une
                div.append(author, date_article); // J'ajoute les éléments author et date dans ma div
                article.append(name, ctn_desc, div); // J'ajoute les éléments name, ctn_desc et div dans mon article
                $("#art_ctn").append(article); // J'ajoute mon article dans mon conteneur à article sur le HTML

                //? Si l'id de l'utilisateur connecté est celui de l'article alors
                if (user.id == art.user_id) {
                    const delbtn = $("<button></button>"); // Je crée un élément bouton
                    delbtn.html("<i class='fa fa-trash' aria-hidden='true'></i>"); // J'ajoute le contenu du bouton, ici une icone de poubelle
                    delbtn.addClass("btn salmon art_btn"); // J'ajoute des classes sur le bouton pour le style
                    
                    // J'ajoute un écouteur d'événement clic sur le bouton
                    delbtn.click(() => {
                        // J'appelle la fonction wantToDelete pour demander la suppression de l'article
                        wantToDelete(art.id);
                    });
                    
                    const updatebtn = $("<button></button>"); // Je crée un élément bouton
                    updatebtn.html("<i class='fa fa-pencil' aria-hidden='true'></i>"); // J'ajoute le contenu du bouton, ici une icone de crayon
                    updatebtn.addClass("btn ocean art_btn"); // J'ajoute des classes sur le bouton pour le style

                    // J'ajoute un écouteur d'événement clic sur le bouton
                    updatebtn.click(() => {
                        // J'appelle la fonction wantToDelete pour demander la suppression de l'article
                        wantToUpdate(art.id);
                    });
                    
                    div.append(updatebtn, delbtn); // J'ajoute mes boutons dans la div de l'article
                }

                article.attr("data-favorite", art.fav);
                const favorite = $("<button></button>"); // Je crée un élément bouton
                const icon_fav = art.fav == 1 ? "<i class='fa fa-star' aria-hidden='true'></i>" : "<i class='fa fa-star-o' aria-hidden='true'></i>";

                favorite.html(icon_fav); // J'ajoute le contenu du bouton, ici une icone d'étoile
                favorite.addClass("btn art_btn favorite"); // J'ajoute des classes sur le bouton pour le style

                // J'ajoute un écouteur d'événement clic sur le bouton
                favorite.click(() => {
                    // J'appelle la fonction favorite pour ajouter ou supprimer l'article en favoris
                    manageFavorite(art.id);
                });

                div.append(favorite);
            });
        } else alert(res.error); //! J'affiche une boite de dialogue avec l'erreur //! J'affiche une boite de dialogue avec l'erreur
    }
});

// Au clic de la div "Ajouter un article"
$("header div.btn").click(() => {
    document.querySelector("form").reset(); // Je vide le formulaire

    article_id = null; // J'affecte à ma variable article_id null car je ne souhaite pas mettre à jour

    $(".box h1").text("Ajouter un article"); // Je mets à jour le titre du formulaire

    $(".box").addClass("open"); // J'affiche mon formulaire
    $("#overlay").addClass("open"); // J'affiche mon overlay
});

$("form").submit((event) => {    
    event.preventDefault(); // J'empêche le comportement par défaut de l'événement. Ici la soumission du formulaire recharge la page

    const name = $("#name").val(); // Je récupère la valeur de mon champs de formulaire du nom de l'article
    const description = $("#desc").val(); // Je récupère la valeur de mon champs de formulaire de la description de l'article
    const picture = $('#picture')[0].files[0]; // Je récupère le fichier du formulaire

    if (article_id) updateArticle(name, description, picture);
    else insertArticle(name, description, picture);
});
