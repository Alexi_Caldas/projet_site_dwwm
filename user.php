<?php

require_once("db_connect.php");

require("function.php");

isConnected();

if ($_SERVER["REQUEST_METHOD"] == "POST") $method = $_POST;
else $method = $_GET;

switch ($method["choice"]) {
    case "select_id":
        $req = $db->prepare("SELECT lastname, firstname, email, FROM user WHERE id=?");
        $req->execute([$_SESSION["user_id"]]);

        $user = $req->fetch(PDO::FETCH_ASSOC);

        echo json_encode(["success" => true, "user" => $user]);
        break;

    case "update":
        if (isset($_POST["lastname"], $_POST["firstanme"], $_POST["email"]) && !empty(trim($_POST["lastname"])) && !empty(trim($_POST["firstname"])) && !empty(trim($_POST["email"]))) {
            $pwdsql = "";
            if (isset($_POST["password"]) && !empty(trim($_POST["password"]))) {
                $pwdsql = ",pwd = :pwd";


                //!regex pour l'email


                $regex = "/^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])[a-zA-Z0-9]{8,12}$/";
                if (!preg_match($regex, $_POST["password"])) {
                    
                    echo json_encode(["success" => false, "error" => "Mot de passe au mauvais format"]);
                    die;
                
                
                }

                $hash = password_hash($_POST["password"], PASSWORD_DEFAULT);
            }

            $req = $db->prepare("UPDATE user SET  lastname = :lastname, firstname = :firstname, email = :email, $pwdsql WHERE id = :id");
            $req->bindValue(":lastname", $_POST["lastname"]);
            $req->bindValue(":firstname", $_POST["firstname"]);
            $req->bindValue(":email", $_POST["email"]);
            if ($pwdsql != "") $req->bindValue(":pwd", $hash);
            $req->bindValue(":id", $_SESSION["user_id"]);
            $req->execute();

            echo json_encode(["success" => true]);
        } else {
            
            echo json_encode(["success" => false, "error" => "Erreur de mise à jour"]);
        }
        break;

    default:
        echo json_encode(["success" => false, "error" => "Demande inconnue"]);
        break;
}