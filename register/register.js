$("form").submit((e) => { 
    e.preventDefault();
    
    $.ajax({
        url: "register.php",
        type: "POST",
        dataType: "json",
        data: {
            lastname: $("#lastname").val(),
            firstname: $("#firstname").val(),
            email: $("#email").val(),
            pwd: $("#pwd").val()
        },
        success: (result) => {
            if (result.success) window.location.replace("../login/login.html");
            else alert(result.error); 
        }
    });
});