<?php

require_once("../db_connect.php");

if (!isset($_POST["lastname"], $_POST["firstname"], $_POST["email"], $_POST["pwd"])) {
    echo json_encode(["success" => false, "error"]);
}

if (
    empty(trim($_POST["lastname"])) ||
    empty(trim($_POST["firstname"])) ||
    empty(trim($_POST["email"])) ||
    empty(trim($_POST["pwd"]))
) {
    echo json_encode(["success" => false, "error" => "Données vides"]);
    die;
}

$regex = "/^[a-zA-Z0-9-+._]+@[a-zA-Z0-9-]{2,}\.[a-zA-Z]{2,}$/";
if (!preg_match($regex, $_POST["email"])) {
    echo json_encode(["success" => false, "error" => "Email au mauvais format"]);
    die;
}

$regex = "/^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])[a-zA-Z0-9]{8,12}$/";
if (!preg_match($regex, $_POST["pwd"])) {
    echo json_encode(["success" => false, "error" => "Mot de passe au mauvais format"]);   
    die;
}

$hashed_pwd = password_hash($_POST["pwd"], PASSWORD_DEFAULT);

$req = $db->prepare("INSERT INTO user (lastname, firstname, email, pwd) VALUES (:lastname, :firstname, :email, :pwd)");
$req->bindValue(":lastname", $_POST["lastname"]);
$req->bindValue(":firstname", $_POST["firstname"]);
$req->bindValue(":email", $_POST["email"]);
$req->bindValue(":pwd", $hashed_pwd);
$req->execute();

echo json_encode(["success" => true]);