$("#lastname").val(user.lastname);
$("#firstname").val(user.firstname);
$("#email").val(user.email);

$("#lastname2").text(user.lastname);
$("#firstname2").text(user.firstname);
$("#email2").text(user.email);


$("form").submit(event => {
    event.preventDefault();

    let lastname = $("#lastname").val();
    let firstname = $("#firstname").val();
    let email = $("#email").val();
    let password = $("#password").val();

    $.ajax({
        url: "../user.php",
        type: "POST",
        dataType: "json",
        data: {
            choice: "update",
            lastname,
            firstname,
            email,
            password
        },
        success: (result) => {
            if (result.success) {
                $("aside p").text(lastname + " " + firstname); 

                user = {
                    lastname,
                    firstname,
                    email
                };

                localStorage.setItem("user", JSON.stringify(user));

                $(".box input[type='submit']").addClass("grass");
            } else {
                $(".box input[type='submit']").addClass("salmon");
                alert(result.error);
            }
        }
    });
});