$("aside p").text(user.lastname + " " + user.firstname);

$(".fa-sharp").click(() => {
    $("aside").addClass("open");
    $("#overlay").addClass("open");
    $(".box").css("display", "none");
});

$("#overlay").click(() => {
    $(".box").css("display", "block");
    $("aside").removeClass("open");
    $("#overlay").removeClass("open");
    if ($(".box").length > 0) $(".box").removeClass("open");
});

if (user.admin == 0) $("#admin_menu").remove();