<?php

require_once("../db_connect.php");
require("../function.php");

isConnected();

isAdmin();

if ($_SERVER["REQUEST_METHOD"] == "POST") $method = $_POST;
else $method = $_GET;

switch ($method["choice"]) {
    case 'select':
        $req = $db->query("SELECT * FROM showroom a INNER JOIN user u ON u.id = a.price_t");
        $showroom = $req->fetchAll(PDO::FETCH_ASSOC);
        echo json_encode(["success" => true, "showroom" => $showroom]);
        break;

    case 'select_id':
        if (isset($_GET["id"])) {
            $req = $db->prepare("SELECT * FROM showroom WHERE id = ?");
            $req->execute([$_GET["id"]]); 
            $showroom = $req->fetch(PDO::FETCH_ASSOC);
            echo json_encode(["success" => true, "showroom" => $showroom]);

        } else {
            echo json_encode(["success" => false, "error" => "Erreur lors de la sélection de la voiture"]);
        }
        break;

    case 'insert':
        if (isset($_POST["name"], $_POST["reference"], $_POST["price_t"]) && !empty(trim($_POST["name"])) && !empty(trim($_POST["reference"])) && !empty(trim($_POST["price_t"]))) {
            
            $req = $db->prepare("INSERT INTO showroom (`name`, reference, price_t) VALUES (:name, :reference, :price_t)");

            $req->bindValue(":name", $_POST["name"]);
            $req->bindValue(":reference", $_POST["reference"]);
            $req->bindValue(":price_t", $_POST["price_t"]);
            $req->execute(); 
            echo json_encode(["success" => true]);

        } else { 
            echo json_encode(["success" => false, "error" => "Erreur lors de l'insertion"]);
        }
        break;

    case 'update':
        if (isset($_POST["name"], $_POST["reference"], $_POST["id"]) && !empty(trim($_POST["name"])) && !empty(trim($_POST["reference"])) && !empty(trim($_POST["id"]))) {
            $req = $db->prepare("UPDATE showroom SET name = ?, reference = ? WHERE id = ?");
            $req->execute([$_POST["name"], $_POST["reference"], $_POST["id"]]);
            echo json_encode(["success" => true]);
        } else {
            echo json_encode((["success" => false, "error" => "Erreur lors de la mise à jour"]));
        }
        break;

    case 'delete':
        if (isset($_POST["id"]) && !empty(trim($_POST["id"]))) {
            $req = $db->prepare("DELETE FROM showroom WHERE id = ?");
            $req->execute([$_POST["id"]]);
            echo json_encode((["success" => true]));

        } else {
            echo json_encode((["success" => false, "error" => "Erreur lors de la suppression"]));
        }
        break;

    default:
        echo json_encode(["success" => false, "error" => "Demande inconnue"]);
        break;
}
