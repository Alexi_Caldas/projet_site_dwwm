<?php

require_once("../db_connect.php");

require("../function.php");

isConnected();

isAdmin();

if ($_SERVER["REQUEST_METHOD"] == "POST") $method = $_POST;
else $method = $_GET;

switch ($method["choice"]) {
    case 'select':
        $req = $db->query("SELECT id, lastname, firstname, email FROM user");

        $users = $req->fetchAll(PDO::FETCH_ASSOC);

        echo json_encode(["success" => true, "users" => $users]);
        break;

    case 'select_id':
        if (isset($_GET["id"])) {
            $req = $db->prepare("SELECT id,  lastname, firstname, email FROM user WHERE id=?");
            $req->execute([$_GET["id"]]);

            $user = $req->fetch(PDO::FETCH_ASSOC);

            echo json_encode(["success" => true, "user" => $user]);
        } else {
            echo json_encode(["success" => false, "error" => "utilisateur inconnu"]);
        }

        break;

    case 'update':
        if (isset($_POST["lastnam"], $_POST["firstname"], $_POST["email"], $_POST["id"]) && !empty(trim($_POST["lastname"])) && !empty(trim($_POST["firstname"])) && !empty(trim($_POST["email"])) && !empty(trim($_POST["id"]))) {
            $req = $db->prepare("UPDATE users SET lastname = ?, firstname = ?, email = ? WHERE id = ?");
            $req->execute([$_POST["lastname"], $_POST["firstname"], $_POST["email"], $_POST["id"]]);

            echo json_encode(["success" => true]);
        } else {
            echo json_encode(["success" => false, "error" => "erreur de la mise à jour"]);
        }
        break;

    case "search":
        if (isset($_GET["search"]) && !empty(trim($_GET["search"]))) {
            $req = $db->prepare("SELECT id, lastname, firstname, email FROM user WHERE lastname LIKE ? OR firstname LIKE ? OR email LIKE ?");
            for ($i = 0; $i < 4; $i++) $data[] = "%{$_GET['search']}%";
            $req->execute($data);

            $users = $req->fetchAll(PDO::FETCH_ASSOC);

            echo json_encode(["success" => true, "users" => $users]);
        } else {
            echo json_encode(["success" => false, "error" => "Données manquantes"]);
        }

        break;

    default:
        echo json_encode(["success" => false, "error" => "Demande inconnue"]);
        break;
}
