if (user.admin == 0) window.location.replace("../home/home.html");

function createRow(data) {
    data.forEach(el => {
        const tr = $("<tr></tr>"); 

        const lastname = $("<td></td>").text(el.lastname); 
        const firstname = $("<td></td>").text(el.firstname); 
        const email = $("<td></td>").text(el.email); 
        
        const link = $("<td></td>");
        const a = $("<a></a>");
        a.html("<i class='fa fa-pencil' aria-hidden='true'></i>");
        a.attr("href", "user_update/user_update.html?id=" + el.id); 
        a.addClass("btn ocean art_btn");
        link.append(a); 

        tr.append(lastname, firstname, email, link);
        $("tbody").append(tr);
    });

    $("td").addClass("text-left");
}

$.ajax({
    url: "user_admin.php",
    type: "GET",
    dataType: "json",
    data: {
        choice: "select"
    },
    success: (result) => {
        createRow(result.users);
    }
});

$("#searchbar").keyup(() => {
    const search = $("#searchbar").val(); 
    
    if (search.length >= 3) {
        $.ajax({
            url: "user_admin.php",
            type: "GET",
            dataType: "json",
            data: {
                choice: "search",
                search
            },
            success: (res) => {
                if (res.success) {
                    $("tbody tr").remove();
                    createRow(res.users);
                } else alert(res.error); 
            }
        });
    }
});