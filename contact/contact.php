<?php

require_once("../db_connect.php");

if ($_SERVER["REQUEST_METHOD"] == "POST") $method = $_POST;
else $method = $_GET;

switch ($method["choice"]) {
    case 'insert':
        if(isset($_POST["lastname"], $_POST["firstname"], $_POST["email"], $_POST["message"]) && !empty(trim($_POST["lastname"])) && !empty(trim($_POST["firstname"])) && !empty(trim($_POST["email"])) && !empty(trim($_POST["message"]))) {
            if (
                empty(trim($_POST["lastname"])) ||
                empty(trim($_POST["firstname"])) ||
                empty(trim($_POST["email"])) ||
                empty(trim($_POST["message"]))
            ) {
                echo json_encode(["success" => false, "error" => "données manquantes"]);
                die;}
            
                $req = $db->prepare("INSERT INTO contact (lastname, firstname, email, `message`) VALUES (:lastname, :firstname, :email, :message)");

                $req->bindValue(":lastname", $_POST["lastname"]);
                $req->bindValue(":firstname", $_POST["firstname"]);
                $req->bindValue(":email", $_POST["email"]);
                $req->bindValue(":message", $_POST["message"]);
                $req->execute();
                echo json_encode((["success" => true]));

            }
        }
