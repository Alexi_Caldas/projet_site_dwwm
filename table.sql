CREATE TABLE supplier (
    id INT(11) auto_increment NOT NULL,
    name VARCHAR(45) NOT NULL,
    country VARCHAR(45) NOT NULL,
    num_str INT(11) NOT NULL,
    nom_str VARCHAR(100) NOT NULL,
    zipcode INT(11) NOT NULL,
    town VARCHAR (45) NOT NULL
);

CREATE TABLE category (
    id INT(11) AUTO_INCREMENT NOT NULL,
    name_category VARCHAR(45) NOT NULL
);

CREATE TABLE product (
    id INT(11) AUTO_INCREMENT NOT NULL,
    name VARCHAR(100) NOT NULL,
    reference VARCHAR(45) NOT NULL,
    price_t DECIMAL(10.0) NOT NULL,
    price_wt DECIMAL(10.0) NOT NULL,
    id_category INT(11) NOT NULL,
    FOREIGN KEY (id_category) REFERENCES category(id)
);

CREATE TABLE user (
    id INT(11) auto_increment NOT NULL,
    lastname VARCHAR(45) NOT NULL,
    firstname VARCHAR(45) NOT NULL,
    number_str INT(11) NOT NULL,    // "str=street"
    name_str VARCHAR(100) NOT NULL, 
    zipcode INT(11) NOT NULL,       
    town VARCHAR (45) NOT NULL,
    email VARCHAR(255) NOT NULL,
    phone_number DECIMAL(10.0) NOT NULL,
    identifiant VARCHAR(45) NOT NULL,
    pwd VARCHAR (75) NOT NULL
);

CREATE TABLE order (
    id IN(11) auto_increment NOT NULL,
    number_str INT(11) NOT NULL,
    name_str VARCHAR(100) NOT NULL,
    zipcode INT(11) NOT NULL,
    town VARCHAR (45) NOT NULL,
    date_order DATE(45) NOT NULL,
    total_price INT(11) NOT NULL,
    FOREIGN KEY (id_user) REFERENCES user(id)
);

CREATE TABLE supply_product ( 
    id_suplier INT(11) NOT NULL,
    id_product INT(11) NOT NULL,
    PRIMARY KEY(id_suplier, id_product),
    FOREIGN KEY(id_suplier) REFERENCES id(suplier),
    FOREIGN KEY(id_product) REFERENCES id(product)
);

CREATE TABLE order_product ( 
    id_order INT(11) NOT NULL,
    id_product INT(11) NOT NULL,
    PRIMARY KEY(id_order, id_product),
    FOREIGN KEY(id_order) REFERENCES id(order),
    FOREIGN KEY(id_product) REFERENCES id(product)
)



    